<?php
include_once './models/constant.php';
use \Models\Constant;

class GoogleRecaptchaMiddleware
{
    public function __invoke($request, $response, $next)
    {
        $constant = Constant::where('name', 'RECAPTCHA_SECRET')->first()->toArray();
        $parsedBody = $request->getParsedBody();
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secret = $constant['value'];
        $myvars = 'secret=' . $secret . '&response=' . $parsedBody['captcha'];

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $respon = curl_exec( $ch );
        echo $respon;
        die();
        $responseArr = json_decode($respon, true);

        if($responseArr['success'] == true) {
            $response = $next($request, $response);
            return $response;
        } else {
            return $response->getBody()
                             ->write(json_encode([
                                 'message' => "wrong captcha",
                                 'error' => $responseArr['error-codes']
                               ])
                             )
                             ->withStatus(401)
                             ->withHeader('Content-type', 'application/json');
        }
    }
}