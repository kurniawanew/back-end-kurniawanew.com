<?php
// register the route below

$app->post('/public/login', '\Controllers\loginController');

$app->get('/public/article/{slug}', '\Controllers\articleController:GetArticleBySlug');
$app->get('/public/article/page/{page}', '\Controllers\articleController:GetAllArticle');

$app->get('/public/tag/{slug}', '\Controllers\tagController:GetArticleByTag');
$app->get('/public/tag', '\Controllers\tagController:GetTags');

$app->get('/public/category/list-article', '\Controllers\categoryController:GetCategoriesAndListArticle');
$app->get('/public/category/{slug}', '\Controllers\categoryController:GetArticleByCategory');
$app->get('/public/category', '\Controllers\categoryController:GetCategories');

$app->get('/public/pages', '\Controllers\pageController:GetPages');

$app->post('/public/comment', '\Controllers\commentController:MakeComment')
	->add(new GoogleRecaptchaMiddleware());

$app->post('/admin/article', '\Controllers\articleController:MakeArticle');