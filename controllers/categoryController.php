<?php
namespace Controllers;

use \Interop\Container\ContainerInterface as ContainerInterface;

include_once './models/category.php';
include_once './models/article.php';
use \Models\Category;
use \Models\Article;

class categoryController 
{
	protected $container;

	public function __construct(ContainerInterface $container) {
	   $this->container = $container;
    }
    
    public function GetCategories($request, $response, $args) {
		$categories = Category::all();

		$response->getBody()->write($categories->toJson());
		return $response->withHeader('Content-type', 'application/json');
    }
    
    public function GetArticleByCategory($request, $response, $args) {
		$article = Category::where('slug', $args['slug'])
							->first()
							->articles()
							->with('tags', 'category', 'comments')
							->get();

		$response->getBody()->write($article->toJson());
		return $response->withHeader('Content-type', 'application/json');
    }
    
    public function GetCategoriesAndListArticle($request, $response, $args) {
        $categories = Category::all()->toArray();

        foreach($categories as $idx => $category) {
            $articles = Article::where('category_id', $category['id'])
                                ->get()
                                ->toArray();
            foreach($articles as $article) {
                $categories[$idx]['articles'][]= [
                    "title" => $article['title'], 
                    "slug" => $article['slug']
                ];
            }
        }

        $response->getBody()->write(json_encode($categories));
		return $response->withHeader('Content-type', 'application/json');
    }
}