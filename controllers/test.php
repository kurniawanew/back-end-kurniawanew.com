<?php
namespace Controllers;

use \Interop\Container\ContainerInterface as ContainerInterface;

include_once './models/user.php';
include_once './models/category.php';
use \Models\User;
use \Models\Category;

class TestController 
{
	protected $container;

   // constructor receives container instance
	public function __construct(ContainerInterface $container) {
	   $this->container = $container;
	}

	public function tes($request, $response, $args) {
		$json = json_encode(["mobil" => ["warna"=>"biru", "jenis"=>"sedan"]]);
		$response->getBody()->write($json);
	    // your code
	    // to access items in the container... $this->container->get('');
	    return $response->withHeader('Content-type', 'application/json');
	}

	public function tes2($request, $response, $args) {
		$decoded = $request->getAttribute("token");
		$response->getBody()->write(json_encode($decoded));
		// $expired = ["expired" => date("Y-m-d H:i:s", $decoded->exp)];
		// $response->getBody()->write(json_encode($expired));
		return $response->withHeader('Content-type', 'application/json');
	}

	public function tes3($request, $response, $args) {
		$user = User::find(1);
		$response->getBody()->write(json_encode($user));
		return $response->withHeader('Content-type', 'application/json');
	}

	public function tes4($request, $response, $args) {
		$user = Category::all();
		$response->getBody()->write(json_encode($user));
		return $response->withHeader('Content-type', 'application/json');
	}
}