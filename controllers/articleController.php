<?php
namespace Controllers;

use \Interop\Container\ContainerInterface as ContainerInterface;

include_once './models/article.php';
include_once './models/user.php';
use \Models\Article;
use \Models\User;

class articleController 
{
	protected $container;

	public function __construct(ContainerInterface $container) {
	   $this->container = $container;
	}

	public function GetAllArticle($request, $response, $args) {
    	$page = isset($args['page']) ? $args['page'] : 1;

		$articles = Article::with('tags:name', 'category:id,name,slug,color')
							->withCount('comments')
							->forPage($page, 15)
							->orderBy('created_at', 'desc')
							->get()
							->toArray();

		foreach ($articles as $idx => $article) {
			$articles[$idx]['content'] = strlen(strip_tags($article['content'])) > 100 ? substr(strip_tags($article['content']), 0, 100) : strip_tags($article['content']);
		}

		$response->getBody()->write(json_encode($articles));
		return $response->withHeader('Content-type', 'application/json');
	}

	public function GetArticleById($request, $response, $args) {
		$article = Article::with('tags', 'category', 'comments')
							->find($args['id']);

		$response->getBody()->write($article->toJson());
		return $response->withHeader('Content-type', 'application/json');
	}

	public function GetArticleBySlug($request, $response, $args) {
		$article = Article::where('slug', $args['slug'])
							->with('tags', 'category', 'comments')
							->first()
							->toArray();

		$user = User::find($article['user_id'])
					  ->makeHidden(['password', 'remember_token', 'created_at', 'updated_at'])
					  ->toArray();
		$article['user'] = $user;

		$response->getBody()->write(json_encode($article));
		return $response->withHeader('Content-type', 'application/json');
	}

	public function MakeArticle($request, $response, $args)
	{
		if (!in_array("admin", $this->container['token']->role)) {
			return $response->getBody()
					 		->write(json_encode(["message" => "You can't access this!"]))
					 		->withStatus(401)
							->withHeader('Content-type', 'application/json');
		}
    	$parsedBody = $request->getParsedBody();
		$article = new Article;
		$article->title = $parsedBody['title'];
		$article->content = $parsedBody['content'];
		$article->category_id = $parsedBody['category_id'];
		$article->slug = $parsedBody['slug'];
		$article->user_id = $parsedBody['user_id'];
		$article->picture = $parsedBody['picture'];
		$article->save();
		$article->tags()->sync($parsedBody['tags'], false);
	}

	function WarnaRandom()
  	{
		$kolor = ['cc0000', 'cc3300', 'cc6600', 'cc9900', '99cc00', '66cc00', '33cc00', '00cc00', '00cc33', '00cc66', '00cc99', '0099cc', '0066cc', '0033cc', '0000cc', '3300cc', '6600cc', '9900cc', 'cc0099', 'cc0066', 'cc0033', 'cc0000'];
        $k = array_rand($kolor,1);
		return $kolor[$k];
	}
}