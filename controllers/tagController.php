<?php
namespace Controllers;

use \Interop\Container\ContainerInterface as ContainerInterface;

include_once './models/tag.php';
use \Models\Tag;

class tagController 
{
	protected $container;

	public function __construct(ContainerInterface $container) {
	   $this->container = $container;
    }
    
    public function GetTags($request, $response, $args) {
		$tags = Tag::all();

		$response->getBody()->write($tags->toJson());
		return $response->withHeader('Content-type', 'application/json');
    }
    
    public function GetArticleByTag($request, $response, $args) {
		$article = Tag::where('slug', $args['slug'])
						->first()
						->articles()
						->with('tags', 'category', 'comments')
						->get();

		$response->getBody()->write($article->toJson());
		return $response->withHeader('Content-type', 'application/json');
	}

}