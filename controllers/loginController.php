<?php
namespace Controllers;

use \Interop\Container\ContainerInterface as ContainerInterface;
use \Firebase\JWT\JWT;

include_once './models/user.php';
use \Models\User;

class loginController
{
  protected $container;

   // constructor receives container instance
  public function __construct(ContainerInterface $container) {
     $this->container = $container;
  }

  public function __invoke($request, $response, $args) {
    $parsedBody = $request->getParsedBody();

    $user = User::where('username', $parsedBody['username'])->get()->first();

    $result = array();
    $role = array();
    $status = 0;
    if (count($user) > 0) {
      $user_roles = $user->role()->get();
      foreach ($user_roles as $user_role) {
        $role[] = $user_role->name;
      }

      $password = sha1("mklajsf&^jgad%#E4" . md5($parsedBody['password']));
      if ($password == $user->password) {
        $result['token'] = $this->getToken($role, $user->id);
        $result['name'] = $user->name;
        $result['email'] = $user->email;
        $result['user_id'] = $user->id;
        $status = 200;
      } else {
        $result['status'] = "error";
        $result['message'] = "Wrong Password";
        $status = 400;
      }
    } else {
      $result['status'] = "error";
      $result['message'] = "Username not registered";
      $status = 400;
    }
    $response->getBody()->write(json_encode($result));
    return $response->withHeader('Content-type', 'application/json')->withStatus($status);
  }

  public function getToken($role, $user_id)
  {
    $issued_at = strtotime("now");
    $expired = strtotime("now +3 hours");

    $secret = "jl^6KemLO5M,$1sH6CVq894g@#K7iLamC&!gk30";

    $payload = [
        "iss" => "http://kurniawanew.com",
        "iat" => $issued_at,
        "exp" => $expired,
        "role" => $role,
        "user_id" => $user_id
    ];

    $token = JWT::encode($payload, $secret, "HS256");
    return $token;
  }

  public function getTokenFromAuth0($request, $response, $args)
  {
    $curl = curl_init();

    $post_field = [
      "client_id" => "gRH36kpfxTShB3c1ONUJ7mBsJvldldBa",
      "client_secret" => "BQGXSS8gMXFQzwMbkycrKEz7Okyz0GDJgOzkthpdM7S6Su80vERDBEkzdkA3YBaK",
      "audience" => "https://kurniawanew.com",
      "grant_type" => "client_credentials",
      "payload" => [
        "user" => [
          "name" => "iwan",
          "id" => 1,
          "email" => "saya@kurniawanew.com"
        ]
      ]
      ];

    // "{\"client_id\":\"gRH36kpfxTShB3c1ONUJ7mBsJvldldBa\",\"client_secret\":\"BQGXSS8gMXFQzwMbkycrKEz7Okyz0GDJgOzkthpdM7S6Su80vERDBEkzdkA3YBaK\",\"audience\":\"https://kurniawanew.com\",\"grant_type\":\"client_credentials\"}"

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://kurniawan.auth0.com/oauth/token",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($post_field),
      CURLOPT_HTTPHEADER => array(
        "content-type: application/json"
      ),
    ));

    $result = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      $response->getBody()->write("cURL Error #:" . $err);
    } else {
      $response->getBody()->write($result);
    }
    return $response->withHeader('Content-type', 'application/json');    
  }
}