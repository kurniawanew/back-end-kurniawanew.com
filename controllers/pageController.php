<?php
namespace Controllers;

use \Interop\Container\ContainerInterface as ContainerInterface;

include_once './models/page.php';

use \Models\Page;

class pageController 
{
	protected $container;

	public function __construct(ContainerInterface $container) {
	   $this->container = $container;
	}

	public function GetPages($request, $response, $args) {
		$pages = Page::all();

		$response->getBody()->write($pages->toJson());
		return $response->withHeader('Content-type', 'application/json');
	}

}