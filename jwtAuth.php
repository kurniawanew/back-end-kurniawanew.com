<?php
$jwtSecret = parse_ini_file("../config.ini")["JWTsecret"];

$container["token"] = function ($container) {
    return new StdClass;
};

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secure" => false,
	"header" => "Authorization",
    "regexp" => "/(.*)/",
    "secret" => $jwtSecret,
    "error" => function ($request, $response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    },
    "path" => ["/"],
    "passthrough" => ["/public"],
    "callback" => function ($request, $response, $arguments) use ($container) {
        $container["token"] = $arguments["decoded"];
    }
]));