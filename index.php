<?php
date_default_timezone_set('Asia/Jakarta');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$config['displayErrorDetails'] = true;

$app = new \Slim\App(["settings" => $config]);

// Set Acces Controll Allow Origin Header
$app->add(function ($request, $response, $next) {
	$response = $next($request, $response);
    return $response->withHeader('Access-Control-Allow-Origin', '*')
    				->withHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
});

$container = $app->getContainer();

require 'error.php';
require 'database.php';
require 'middleware.php';
require 'controllers.php';

// call route.php file
require 'route.php';

// Authentication setting for using JWT
require 'jwtAuth.php';

// run SLIM
$app->run();