<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

include_once 'article.php';

class Category extends Model
{
	public function articles()
	{
		return $this->hasMany('\Models\Article');
	}
}