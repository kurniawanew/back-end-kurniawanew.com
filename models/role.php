<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

require_once 'user.php';

class Role extends Model
{
	public function user()
	{
		return $this->belongsToMany('\Models\User');
	}
}