<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

require_once 'role.php';

class User extends Model
{
	public function role()
	{
		return $this->belongsToMany('\Models\Role');
	}
}