<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

include_once 'article.php';

class Tag extends Model
{
	public function articles()
	{
		return $this->belongsToMany('\Models\Article');
	}
}