<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

include_once 'user.php';
include_once 'tag.php';
include_once 'category.php';
include_once 'comment.php';

class Article extends Model
{
    public function user()
    {
        return $this->belongsTo('\Models\User');
    }

	public function tags()
    {
        return $this->belongsToMany('\Models\Tag');
    }

    public function category()
    {
        return $this->belongsTo('\Models\Category');
    }

    public function comments()
    {
        return $this->hasMany('\Models\Comment');
    }
}