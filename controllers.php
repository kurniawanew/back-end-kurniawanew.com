<?php 
// All controllers must be registered on this file

require 'controllers/test.php';
require 'controllers/loginController.php';
require 'controllers/articleController.php';
require 'controllers/commentController.php';
require 'controllers/pageController.php';
require 'controllers/categoryController.php';
require 'controllers/tagController.php';