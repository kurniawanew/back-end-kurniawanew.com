<?php
$config = parse_ini_file("../config.ini");

$container['settings']['db'] = 
	[
	    'driver' => 'mysql',
        'host' => $config['db_host'],
        'database' => $config['db_database'],
        'username' => $config['db_user'],
        'password' => $config['db_password'],
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
	];

// Service factory for the ORM
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);

$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) {
    return $capsule;
};