<?php
// for Internal Server Error 500
// $container['errorHandler'] = function ($container) {
//     return function ($request, $response, $exception) use ($container) {
//         return $container['response']->withStatus(500)
//                              ->withHeader('Content-Type', 'application/json')
//                              ->write(json_encode(['message' => 'Something went wrong!']));
//     };
// };

// for Page Not Fount 404
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(['message' => 'Page not found']));
    };
};

// for Not Allowed 405
// $container['notAllowedHandler'] = function ($container) {
//     return function ($request, $response, $methods) use ($container) {
//         return $container['response']
//             ->withStatus(405)
//             ->withHeader('Allow', implode(', ', $methods))
//             ->withHeader('Content-type', 'application/json')
//             ->write(json_encode(['message' => 'Method must be one of: ' . implode(', ', $methods)]));
//     };
// };